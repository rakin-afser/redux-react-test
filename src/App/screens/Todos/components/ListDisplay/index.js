import React, { PropTypes } from 'react'
import List from '../List'
const ListDisplay=({lists,todos,toggleTodo,addTodo,filterTodos,filters})=>{

  return (
    <ul className='list pl0 ml0 center mw6 ba b--light-silver br2'>
      {lists.length > 0
        ? lists.map((list,i)=>(
          <List
            key={i}
            text={list.name}
            obj={list}
            toggleTodo={toggleTodo}
            todos={todos.filter((todo,i)=>{
              return todo.listID === list.id
            })}
            addTodo={addTodo}
            filterTodos={filterTodos}
            filters={filters}
          >

          </List>
        ))
        : <p className='ph3 pv3 tc'>No List found</p>
      }
    </ul>
  )
}
export default ListDisplay
