import React, { PropTypes,Component } from 'react'

export default class Checkbox extends Component {
  constructor(props) {
    super(props)
    this.handleCheckbox=this.handleCheckbox.bind(this)
  }
  handleCheckbox(event){
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.props.filterTodos(this.props.listID,value)
  }
  render(){

    return (
      <p><input name='completed' type='checkBox' onChange={this.handleCheckbox} checked={this.props.checked}/>Completed </p>
    )
  }
}
