import React, { PropTypes,Component } from 'react'

import classNames from 'classnames'
import TodoList from '../TodoList'
export default class List extends Component{
  constructor(props) {
    super(props)
    this.state={
      selectedList:''
    }
    this.selectList=this.selectList.bind(this)
  }
  selectList(id){
    var previousId=this.state.selectedList
    this.setState({
      selectedList:previousId === id ? '':id
    })
  }

  render(){
    const listClass = classNames(
      'ph3 pv3 pointer bg-animate hover-bg-light-gray'

    )
    var _that=this;


    var todos=this.props.todos
    var toggleTodo=this.props.toggleTodo;
    var addTodo=this.props.addTodo
    var listID=this.state.selectedList
    var filterTodos=this.props.filterTodos
    var filters=this.props.filters
    return(
      <div>
      <li className={listClass} onClick={()=>{
        _that.selectList(_that.props.obj.id)
      }}  >{this.props.text} </li>
      {this.state.selectedList !== '' ? <TodoList {...{todos,toggleTodo,addTodo,listID,filterTodos,filters}}  /> : ''}
      </div>
    )
  }
}
