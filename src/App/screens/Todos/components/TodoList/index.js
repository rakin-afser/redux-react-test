import React, { PropTypes } from 'react'
import Checkbox from '../Checkbox'
import Todo from '../Todo'
import AddTodo from '../AddTodo'
const sortByDate = (arr) => arr.sort((a, b) => {
  // Turn your strings into dates, and then subtract them
  // to get a value that is either negative, positive, or zero.
  return new Date(b.createdAt) - new Date(a.createdAt)
})

const TodoList = ({ todos, toggleTodo,addTodo,listID,filterTodos,filters }) => {

  var newTodos=todos
  var checked=filters[listID]
  if(filters[listID] === true ){
    newTodos=todos.filter(function(todo,index){

      return todo.completed === true
    })
  }

  const sortedTodos = todos && todos[0] ? sortByDate(todos) : null
  

  return (
    <div>
      {todos.length > 0 ? <Checkbox {...{filterTodos,listID,checked}} />: ''}
      <ul className='list pl0 ml0 center mw6 ba b--light-silver br2'>
        {sortedTodos
          ? newTodos.map((todo, i) =>
            <Todo
              key={i}
              {...todo}
              toggle={() => toggleTodo(todo, !todo.completed)}
              isLast={(todos.length - 1) === i}

            />
          )
          : <p className='ph3 pv3 tc'>No todos found</p>
        }
      </ul>
      <AddTodo onSubmit={({todo}, _, {reset}) => {
        addTodo(todo,listID)
        reset()
      }} ></AddTodo>
    </div>
  )
}

TodoList.propTypes = {
  todos: PropTypes.array
}

export default TodoList
 // filterTodos={filterTodos} listID={listID} checked={checked}
