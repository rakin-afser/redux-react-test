import React, { PropTypes } from 'react'
import { connect } from 'react-redux'

import * as actions from 'App/stores/resources/actions'
import { getEntities } from 'App/stores/resources'

import AddTodo from './components/AddTodo'
import TodoList from './components/TodoList'
import AddList from './components/AddList'
import ListDisplay from './components/ListDisplay'
const Todos = ({ todos, addTodo, toggleTodo,addList, lists,filterTodos,filters}) => {
  var arg={todos,toggleTodo}
  return (


      <section className='pa3 pa5-ns'>
        <AddList onSubmit={(val,_,{reset})=>{

          addList(val)
          reset()
        }} />



        <h1 className='f4 bold center mw6'>All List</h1>

        <ListDisplay {...{ lists,todos,toggleTodo,addTodo,filterTodos ,filters}} />

      </section>

  )
}

Todos.propTypes = {
  todos: PropTypes.array
}

export default connect(
  state => ({
    todos: getEntities('todos')(state),
    lists: getEntities('lists')(state),
    filters:state.lists.filters,
  }),
  dispatch => ({
    addTodo: (text,listID) => dispatch(actions.submitEntity({ text,listID }, {type: 'todos'})),
    toggleTodo: (todo, completed) => dispatch(actions.updateEntity({ ...todo, completed }, {type: 'todos'})),
    addList:(text) => dispatch(actions.submitEntity({ ...text }, {type: 'lists'})),
    filterTodos: (listID,value) => dispatch(actions.filterTodos({listID,value}, {type: 'filter'}))
  })
)(Todos)
