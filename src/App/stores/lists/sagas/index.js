
import * as schema from '../schema'

/*
 * Subroutines
 */

export function* receiveResponse (response) {
  if (response.ok) {
    const list = normalize(response.data.list, schema.list)
    console.log('receiveResponse in todo_saga_middleware ',response,list);
    yield put(actions.setEntity(list, {type: 'lists'}))
  } else {
    const error = response.data.error

    yield put(actions.requestFailure(error, {type: 'lists'}))
  }
}

export function* addList(){
  console.log('print list in todo_saga_middleware ');
  while (true) {
    action = yield take(t.SUBMIT_ENTITY)
    if (action.meta && action.meta.type === 'lists') {
      const list = {
        ...action.payload,
      }
      console.log('print list in todo_saga_middleware ',{...list},action);
      const response = yield call(api.post, '/lists', {...list})
      yield fork(receiveResponse,response)
    }
  }
}

export default function* watchLists () {
  yield [
    fork(addList)
  ]
}
